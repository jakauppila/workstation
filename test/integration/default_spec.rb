describe command 'cinc-client --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command 'cinc-solo --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command 'chef-client --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Redirecting to cinc-client/ }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command 'chef-solo --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Redirecting to cinc-solo/ }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command '/opt/cinc-workstation/embedded/bin/cinc-zero --version' do
  its('exit_status') { should eq 0 }
end

describe command 'cinc-auditor version' do
  its('exit_status') { should eq 0 }
end

describe command 'cinc-auditor detect' do
  its('exit_status') { should eq 0 }
end

describe command 'inspec version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Redirecting to cinc-auditor/ }
end

describe command '/opt/cinc-workstation/embedded/bin/mixlib-install list-versions cinc stable' do
  its('exit_status') { should eq 0 }
end
